#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

pub trait Shape {
    fn get_block(&self) -> bool;
}

pub struct Container {
    width: i64,
    height: i64,
}

impl Shape  for Container {
    fn get_block(&self) -> bool {
        false
    }
}