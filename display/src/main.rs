extern crate gio;
extern crate gtk;
use gio::prelude::*;
use gtk::prelude::*;

fn main() {
    let ui = gtk::Application::new(
        Some("com.gitlab.tony84727.buildercalculator"),
        gio::ApplicationFlags::FLAGS_NONE,
    )
    .expect("Application::new failed");
    ui.connect_activate(|app| {
        let win = gtk::ApplicationWindow::new(app);
        win.set_default_size(800, 640);
        win.set_title("Builder Calculator");
        win.show_all();
    });

    ui.run(&std::env::args().collect::<Vec<_>>());
}
