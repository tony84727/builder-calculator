use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Option {
    shape: String,
    width: i64,
    height: i64,
}

fn main() {
    let args = Option::from_args();
    println!("{:?}", args);
}
